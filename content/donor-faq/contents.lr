_model: donor-faq
---
_template: donor-faq.html
---
section_id: donate
---
section: Donate
---
color: primary
---
title: Donation FAQ
---
faq_entries:

#### faq-entry ####
question: How can I donate to the Tor Project?
----
answer: * [Amazon Smile](https://smile.amazon.com/ch/20-8096820)
* [Bank Transfer (in the United States only)](#can-i-make-a-donation-via-bank-transfer)
* [Cash, Check, or Money Order](#can-i-donate-with-a-check-cash-or-money-order)
* [Credit Card](https://donate.torproject.org)
* [Cryptocurrency via BTCPayServer](https://donate.torproject.org/cryptocurrency)
* [Cryptocurrency via wallet address](https://donate.torproject.org/cryptocurrency)
* [eBay for Charity](https://www.charity.ebay.com/charity/The-Tor-Project/169077)
* [Stock Donation](#can-i-donate-stock)
* [Open Collective (USD $)](https://opencollective.com/thetorproject), [Open Collective (EUR €)](https://opencollective.com/thetorproject-europe)
* [PayPal](https://donate.torproject.org)

If you need assistance making a gift with any of the methods above, please email giving(at)torproject.org.
#### faq-entry ####
question: How can I get a refund for my donation?
----
answer: [Read our refund policy for fiat, cryptocurrency, and other circumstances.](/refund-policy/)

#### faq-entry ####
question: Do I need to make a donation to use Tor?
----
answer:
***No. Tor Browser and all services developed by the Tor Project are completely free to use.*** If someone is charging you to use their app or service, it's not a tool from the Tor Project, and we cannot speak to its validity or safety.

#### faq-entry ####
question: Help! Donating by credit card isn't working
----
answer:
We’re aware of a few issues with our current donation form that may prevent you from giving by credit card. While we’re working on fixing these, there are a few workarounds you can try in the meantime:

1. **Allow third-party cookies on _donate.torproject.org_ only** ([**here’s how to do it in Firefox**](https://support.mozilla.org/en-US/kb/third-party-cookies-firefox-tracking-protection)). Disabling third-party cookies blocks Stripe, our credit card payment processor, from processing your donation. If you’re using any third-party privacy extensions like Privacy Badger, temporarily deactivate these and try again. Please remember to re-enable your extensions after your donation has been completed. If you've tried the above and still can't donate by credit card, please [report the issue on the Tor forum](https://forum.torproject.net/t/donating-via-credit-card-issue/724).
2. **Re-enable JavaScript.** Stripe and PayPal both require JavaScript to work. In order to donate by credit card you may consider whitelisting donate.torproject.org or temporarily enabling JavaScript. Tor Browser users on the Safest security level can do so by downgrading to Safer in about:preferences#privacy.
3. **For users logging into PayPal with Tor Browser, try another browser.** PayPal cannot process donations made via the Tor Network. We apologize for the inconvenience.

If you’d rather not try any of the workarounds described above, we understand! You can still [donate cryptocurrency](https://donate.torproject.org/cryptocurrency), [send checks and money orders by mail](https://donate.torproject.org/donor-faq/#can-i-donate-by-mail), or and make a donation via Open Collective in [USD](https://opencollective.com/thetorproject) or [EUR](https://opencollective.com/thetorproject-europe). If you need any further help, please email giving(at)torproject.org.

#### faq-entry ####
question: How will you use my donation?
----
answer:
We use unrestricted donations to keep Tor strong, improve our software, and help millions of people browse the internet privately and anonymously. You can learn more about the Tor Project's funding on our [Reports page](https://www.torproject.org/about/reports/) by viewing U.S. 990 Tax Forms and audited financial statements.

#### faq-entry ####
question: Is my donation tax-deductible?
----
answer:
If you pay taxes in the United States, your donation is tax deductible to the full extent required by law. You may need the following information for reporting purposes:

**Tax ID Number (EIN):** 20-8096820<br>
**Address:** The Tor Project, Inc., PO Box 5, Winchester, NH 03470 USA<br>
**Phone number:** (603) 852-1650<br>
**Contact person:** Isabela Dias Fernandes, Executive Director

#### faq-entry ####
question: Can I make a donation if I don't live in the United States?
----
answer:
There are many ways you can make a donation from outside of the U.S.:

You can [donate directly to the Tor Project in USD ($) via our donate page](https://donate.torproject.org), but your donation won't be tax-deductible unless you pay taxes on U.S. income.

You can [donate to the Tor Project in EUR (€) via Open Collective](https://opencollective.com/thetorproject-europe), who will issue a receipt for tax purposes for donations of €10,000 or more.

You can [donate to a relay operator association organization in your region](https://community.torproject.org/relay/community-resources/relay-associations/). These organizations are not the same as The Tor Project, Inc., but we consider that a good thing. They're run by nice people who are part of the Tor community, and your donation may be tax-deductible in your country.

#### faq-entry ####
question: Can I make a donation via bank transfer?
----
answer:
In the U.S.: We can only accept donations via bank transfer for gifts more than $1,000 USD. If you would like to make a bank transfer of more than $1,000 to the Tor Project, please contact giving(at)torproject.org with your name and phone number and Tor Project will reach out to you to coordinate your donation.

#### faq-entry ####
question: Can I donate with a check, cash, or money order?
----
answer:
Yes. Send checks, cash, or money orders to the Tor Project, Inc., PO Box 5, Winchester NH 03470, USA.

#### faq-entry ####
question: Can I donate stock?
----
answer:
Yes. Please contact us in advance at giving(at)torproject.org if you are transmitting securities via DTC.

**Brokerage Firm:** Merrill Lynch, Pierce, Fenner & Smith, Inc.<br>
**Account Name:** The Tor Project<br>
**Contact:** Jeffrey K. Miller: 206.464.3564<br>
**Email:** jeffrey.k.miller(at)ml.com<br>
**FAX:** 206.388.4456<br>
**Account #:** \*\*\*-\*3016<br>
**DTC #:** 8862<br>
**Tax ID #:** 20-8096820

#### faq-entry ####
question: Why is there a minimum donation?
----
answer: The minimum donation helps deter fraud.

#### faq-entry ####
question: Is there a maximum donation?
----
answer: No.

#### faq-entry ####
question: Can I restrict my donation to a particular purpose or project?
----
answer: No, sorry. However, we would be happy to hear your ideas and feedback about our work.

#### faq-entry ####
question: Does Tor Project accept matching donations?
----
answer: Yes! Many companies—such as Google, Microsoft, eBay, PayPal, Apple, Verizon, Red Hat, many universities, and others—will match donations made by their employees. The fastest way to find out if your company matches donations is by checking with your HR department.

#### faq-entry ####
question: Can I donate my time?
----
answer: Yes. [Learn more about joining the Tor community](https://community.torproject.org/).

#### faq-entry ####
question: Can I donate my airline miles, flight vouchers, or hotel points?
----
answer: No, sorry.

#### faq-entry ####
question: Can I donate hardware?
----
answer: No, sorry.

#### faq-entry ####
question: I would like my company to support Tor. What can we do to help?
----
answer: The easiest way to support the Tor Project financially as a company is through [the Tor Project Membership Program](https://torproject.org/about/membership). <br>
Your company can also run a Tor relay or bridge. If your company sells cloud services, perhaps it could donate these to Tor.

#### faq-entry ####
question: When will I get my stickers, t-shirt, or hoodie?
----
answer: We are a small nonprofit. It can take a few weeks to receive your gift—and more if you are outside of the U.S. After six weeks, please email us at giving(at)torproject.org if you would like to check on the status of your gift.

#### faq-entry ####
question: I donated cryptocurrency, where is my receipt?
----
answer: You will not receive an automatically generated receipt if you donate any form of cryptocurrency. If you require a receipt, contact us at giving(at)torproject.org with verification of your transaction.

#### faq-entry ####
question: I donated cryptocurrency, can I get stickers, t-shirts, or hoodies?
----
answer:
Yes. Contact us at giving(at)torproject.org with the following:
* Your mailing address
* Your swag selection, including size (see[ https://donate.torproject.org](https://donate.torproject.org/) for current options)
* Verification of your transaction

#### faq-entry ####
question: How do I cancel my monthly donation?
----
answer:
If you have your monthly donation set up through PayPal, you can cancel your donation by logging into PayPal.

If you have your monthly donation set up to come directly from your credit card, please contact giving(at)torproject.org with the following information:
* Your name
* The email address used when you set up the original monthly donation
* The amount you donate monthly

If you were mailed a thank-you gift as a result of your monthly donation, your refund will be limited to the amount of the donation above the gift giving level. For instance, if the giving level is $75 to receive a T-shirt and you donated $75, we will not process a refund; however, if you donated $100, we would process a $25 refund.

#### faq-entry ####
question: What is your donor privacy policy?
----
answer: [Here is the Tor Project donor privacy policy](https://donate.torproject.org/privacy-policy/).

#### faq-entry ####
question: In your privacy policy, you say you will never publicly identify me as a donor without my permission. What does that mean?
----
answer: If you donate to the Tor Project, there will be some people at the Tor Project who know about your donation. However, we will never publicly identify you as a donor, unless you have given us permission to do so. That means we won't post your name on our website, thank you on social media or other public forums, nor do anything else that would publicly identify you as someone who has donated to the Tor Project.

If, for any reason, we would like to publicly name you as a donor, we will ask you first, and will not do it until and unless you say it's okay. If you talk about your donation on social media (and we appreciate you doing so!), we will take that to mean it's okay for us to engage with or amplify your post.

#### faq-entry ####
question: Why do you ask for my address and similar information?
----
answer: If you donate by credit card, you will be asked for some information that's required to process your credit card payment, including your billing address. This allows our payment processor to verify your identity, process your payment, and prevent fraudulent charges to your credit card. We don't ask for information beyond what's required by the payment processor.

#### faq-entry ####
question: If I want my donation to be anonymous, what is the best way for me to donate?
----
answer: You can donate by sending us cash or a postal money order. You can donate via cryptocurrency if you have it set up in a way that preserves your anonymity. You can buy cash gift cards and mail them to us. There are probably other ways to donate anonymously that we haven't thought of—maybe you will.

#### faq-entry ####
question: Is the Tor Project required to identify me as a donor to the United States government, or to any other authority?
----
answer: If you donate above a certain amount (roughly $80,000 USD as of 2020) to the Tor Project in a single year, we are required to report the donation amount and your name and address (if we have it) to the IRS, on Schedule B of the Form 990, which is filed annually.

However, it's normal for nonprofits to redact individual donor information from the copy of the 990 that's made publicly-available on our website, and that's what we do.

The dollar amount at which we are required to report gifts changes annually, as it is calculated based on the Tor Project's total revenue for the year, so please contact us at giving(at)torproject.org if you have questions about ensuring that your gift falls under this threshold.

#### faq-entry ####
question: Where can I view U.S. state disclosure statements?
----
answer: Certain U.S. states require written disclosures for nonprofit organizations soliciting contributions. [Read individual state disclosures here](https://donate.torproject.org/state-disclosures).

#### faq-entry ####
question: I have questions about how Tor works, where can I learn more?
----
answer: Visit our [Support FAQ](https://support.torproject.org/faq/).
