import React from 'react';

export function FrequencyOptionButton(props) {
  const {frequency, onFrequencySelection, name} = props;

  var label = 'Once';
  if (name == 'monthly') {
    label = 'Monthly';
  }

  var classes = ['btn', name];
  if (name == frequency) {
    classes.push('selected');
  }

  return (
      <React.Fragment>
        <button
          name={name}
          className={classes.join(' ')}
          onClick={() => onFrequencySelection(name)}
        >
          Donate <span className="bold">{label}</span>
        </button>
      </React.Fragment>
  );
}
