export function buildUrl(appContext, path) {
  const {assetBaseUrl} = appContext;
  return `${assetBaseUrl.path}${path}?${assetBaseUrl.queryString}`;
}
