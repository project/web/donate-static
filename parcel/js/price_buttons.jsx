import React from 'react';

import {showCommaForThousands} from './number';

export function PriceButtons(props) {
  const {onPriceChange, selectedPrice, pricesOnButtons} = props;
  const prices = pricesOnButtons.filter(price => !isNaN(price));

  const onClick = (event) => {
    onPriceChange(event.target.getAttribute('name'));
  }

  function renderPriceButton(price) {
    const classes = ['price-btn'];
    if (price == selectedPrice) {
      classes.push('selected');
    }
    return (
      <button
        type="button"
        className={classes.join(' ')}
        key={price}
        name={price}
        onClick={onClick}
      >
        ${showCommaForThousands(price/100)}
      </button>
    );
  }

  return (prices.map((price) => renderPriceButton(price)));
}
