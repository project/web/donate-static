import React from 'react';
import {findErrorByName} from './named_error';

export function RegionDropdown(props) {
  const {regions, selectedCountry, required, onChange, errors} = props;

  let classes=['field'];
  if (findErrorByName(errors, 'region') != undefined) {
    classes.push('error');
  } else {
    classes.push('required');
  }

  const regionsForCountry = regions[selectedCountry];
  if (regionsForCountry == undefined) {
    return (
      <select id="region" name="region" className={`${classes.join(' ')} d-none`} onChange={onChange}>
        <option key="none" value=""></option>
      </select>
    );
  } else {
    let optionElements = [
      (<option key="none" value="">State</option>),
    ];
    for (const region of regionsForCountry) {
      optionElements.push(<option key={region} value={region}>{region}</option>);
    }
    return(
      <select id="region" name="region" className={classes.join(' ')} onChange={onChange}>
        {optionElements}
      </select>
    );
  }
}
